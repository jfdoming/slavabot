package com.ekkongames.slavabot;

/**
 * @author Julian Dominguez-Schatz <jfdoming at ekkon.dx.am>
 */
public interface PermissionConstants {
    // roles
    String OWNER_ROLE = "Owner";
    String MODERATOR_ROLE = "Moderator";

    // role IDs
    String AWESOMENAUTS_ROLE_ID = "356277230458109957";
}
